## Description
The program finds a minimum of functions using the genetic algorithm.

## Examples
![picture](screenshots/GA1.png)
![picture](screenshots/GA2.png)
![picture](screenshots/GA3.png)