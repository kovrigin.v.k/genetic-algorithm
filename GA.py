from PyQt5 import QtCore, QtGui, QtWidgets
from itertools import combinations
import pyqtgraph as pg
import numpy as np
import copy


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1400, 900)
        MainWindow.setWindowIcon(QtGui.QIcon("logo.png"))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.GridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.GridLayout.setObjectName("GridLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.NumIndivid = QtWidgets.QLineEdit(self.centralwidget)
        self.NumIndivid.setObjectName("NumIndivid")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.NumIndivid)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.Epohes = QtWidgets.QLineEdit(self.centralwidget)
        self.Epohes.setObjectName("Epohes")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.Epohes)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.MutateProp = QtWidgets.QLineEdit(self.centralwidget)
        self.MutateProp.setObjectName("MutateProp")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.MutateProp)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.NumKills = QtWidgets.QLineEdit(self.centralwidget)
        self.NumKills.setObjectName("NumKills")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.NumKills)
        self.Functions = QtWidgets.QComboBox(self.centralwidget)
        self.Functions.setObjectName("Functions")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.SpanningRole, self.Functions)
        self.GridLayout.addLayout(self.formLayout, 0, 0)

        self.btn = QtWidgets.QPushButton(self.centralwidget)
        self.btn.setObjectName("btn")
        self.GridLayout.addWidget(self.btn, 1, 0)

        self.Protocol = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.Protocol.setObjectName("Protocol")
        self.GridLayout.addWidget(self.Protocol, 2, 0)

        self.btnClearProtocol = QtWidgets.QPushButton(self.centralwidget)
        self.btnClearProtocol.setObjectName("btnClearProtocol")
        self.GridLayout.addWidget(self.btnClearProtocol, 3, 0)

        self.btnClearPlot = QtWidgets.QPushButton(self.centralwidget)
        self.btnClearPlot.setObjectName("btnClearPlot")
        self.GridLayout.addWidget(self.btnClearPlot, 4, 0)

        self.graph = pg.PlotWidget(self.centralwidget)
        self.graph.setObjectName("graph")
        self.GridLayout.addWidget(self.graph, 0, 1, 5, 1)
        self.GridLayout.setColumnStretch(0, 1)
        self.GridLayout.setColumnStretch(1, 2)

        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.setupAddUiConfig()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Генетический алгоритм"))
        self.label.setText(_translate("MainWindow", "Количество особей:"))
        self.label_2.setText(_translate("MainWindow", "Количество поколений:"))
        self.label_3.setText(_translate("MainWindow", "Вероятность мутаций:"))
        self.label_4.setText(_translate("MainWindow", "Коэффициент убийства особей:"))
        self.Functions.addItems(["x^2 + 3*y^2 + 2*x*y", "100*(y - x^2)^2 + (1-x)^2",
                                 "-12*y + 4*x^2 + 4*y^2 - 4*x*y", "(x - 2)^4 + (x - 2*y)^2",
                                 "4*(x - 5)^2 + (y - 6)^2", "(x - 2)^4 + (x - 2*y)^2",
                                 "2*x^3 + 4*x*y^3 - 10*x*y + y^2", "8*x^2 + 4*x*x + 5*x^2",
                                 "(y-x^2)*^2 + (1 - x)^2", "(y - x^2)^2 + 100*(1 - x^2)^2",
                                 "x^3 + y^2 - 3*x - 2*y + 2"])
        self.btn.setText(_translate("MainWindow", "Найти минимум функции"))
        self.btnClearProtocol.setText(_translate("MainWindow", "Очистить протокол"))
        self.btnClearPlot.setText(_translate("MainWindow", "Очистить график"))

    def setupAddUiConfig(self):
        self.dim = 2
        intValidator = QtGui.QIntValidator()
        self.NumIndivid.setValidator(intValidator)  # устанавливаем возможность записывать в VertexNum только чисел
        self.NumIndivid.setText("100")
        self.Epohes.setValidator(intValidator)
        self.Epohes.setText("101")

        doubleValidator = QtGui.QDoubleValidator()
        self.MutateProp.setValidator(doubleValidator)
        self.MutateProp.setText("0.8")
        self.NumKills.setValidator(doubleValidator)
        self.NumKills.setText("0.2")

        self.btn.clicked.connect(self.Solve)
        self.btnClearProtocol.clicked.connect(self.ClearProtocolMethod)
        self.btnClearPlot.clicked.connect(self.ClearPlotMethod)

    def Solve(self):

        individ = int(self.NumIndivid.text())
        max_epoch = int(self.Epohes.text())
        self.mutation_probability = float(self.MutateProp.text())
        self.portion = float(self.NumKills.text())
        self.funcnum = self.Functions.currentIndex()

        max_pairs = 1000
        minx = -20
        maxx = 20
        self.population = []
        self.population = np.random.randint(minx, maxx, (individ, 2)).tolist()
        x = np.linspace(minx, maxx, 200)
        y = funcForGraph(x, self.funcnum)
        self.graph.plot(x, y)
        xdots = []
        for j in range(len(self.population)):
            xdots.append(self.population[j][0])
        xdots = np.array(xdots)
        ydots = funcForGraph(xdots, self.funcnum)
        s1 = pg.ScatterPlotItem(xdots, ydots, size=10, pen=pg.mkPen(None), brush=pg.mkBrush(0, 255, 0, 200))
        self.graph.addItem(s1)
        QtGui.QApplication.processEvents()

        epoch = 0
        while epoch < max_epoch:
            if epoch % 10 == 0 and epoch > 1:
                self.Protocol.appendPlainText("Поколение = " + str(epoch))
                QtGui.QApplication.processEvents()
            epoch += 1
            ind = 0
            newpopulation = copy.copy(self.population)
            for comb in combinations(range(len(self.population)), self.dim):
                ind += 1
                if ind > max_pairs:
                    break
                a = self.mutate(self.population[comb[0]])
                b = self.mutate(self.population[comb[1]])
                newitem = self.crossover(a, b)
                newpopulation.append(newitem)
            self.population = self.killing(newpopulation)
            xdots = []
            for j in range(len(self.population)):
                xdots.append(self.population[j][0])
            xdots = np.array(xdots)
            ydots = funcForGraph(xdots, self.funcnum)
            ydots = np.array(ydots)
            s1.setData(xdots, ydots)
            QtGui.QApplication.processEvents()
        fmin = 10000
        xmin = 0
        ymin = 0
        for x, y in self.population:
            f = func([x, y], self.funcnum)
            if f < fmin:
                fmin = f
                xmin = x
                ymin = y
        self.Protocol.appendPlainText("Минимум функции = %2.f" % fmin + "\n"
                                      + " в точках: " + str([xmin, ymin]) + "\n")

    def killing(self, population):
        res = np.argsort([func(item, self.funcnum) for item in population])
        res = res[:np.random.poisson(int(len(population) * self.portion))]
        return np.array(population)[res].tolist()

    # exchange of values (crossingover), occurs with a probability of 0.5 by default
    def crossover(self, a, b, prob=0.5):
        if np.random.rand() > prob:
            return [a[0], b[1]]
        else:
            return [b[0], a[1]]

    # mutation of values occurs with a certain probability (0.8 - by default)
    def mutate(self, a):
        if np.random.rand() < self.mutation_probability:
            newa = a + (np.random.rand(2) - 0.5) * 0.05
        else:
            newa = a
        return newa

    def ClearProtocolMethod(self):
        self.Protocol.clear()

    def ClearPlotMethod(self):
        self.graph.getPlotItem().clear()


def funcForGraph(x, func_number):
    if func_number == 0:
        return x ** 2 + 3 * x ** 2 + 2 * x * x
    if func_number == 1:
        return 100 * (x - x ** 2) ** 2 + (1 - x) ** 2
    if func_number == 2:
        return -12 * x + 4 * x ** 2 + 4 * x ** 2 - 4 * x * x
    if func_number == 3:
        return (x - 2) ** 4 + (x - 2 * x) ** 2
    if func_number == 4:
        return 4 * (x - 5) ** 2 + (x - 6) ** 2
    if func_number == 5:
        return (x - 2) ** 4 + (x - 2 * x) ** 2
    if func_number == 6:
        return 2 * x ** 3 + 4 * x * x ** 3 - 10 * x * x + x ** 2
    if func_number == 7:
        return 8 * x ** 2 + 4 * x * x + 5 * x ** 2
    if func_number == 8:
        return (x - x ** 2) ** 2 + (1 - x) ** 2
    if func_number == 9:
        return (x - x ** 2) ** 2 + 100 * (1 - x ** 2) ** 2
    if func_number == 10:
        return x ** 3 + x ** 2 - 3 * x - 2 * x + 2
    if func_number == 11:
        return (x - 1) ** 2 + (x - 3) ** 2 + 4 * (x + 5) ** 2
    if func_number == 12:
        return 3 * (x - 4) ** 2 + 5 * (x + 3) ** 2 + 7 * (2 * x + 1) ** 2


def func(pos, func_number):  # 4(x1 – 5)2 + (x2 – 6)2
    if func_number == 0:
        return pos[0] ** 2 + 3 * pos[1] ** 2 + 2 * pos[0] * pos[1]
    if func_number == 1:
        return 100 * (pos[1] - pos[0] ** 2) ** 2 + (1 - pos[0]) ** 2
    if func_number == 2:
        return -12 * pos[1] + 4 * pos[0] ** 2 + 4 * pos[1] ** 2 - 4 * pos[0] * pos[1]
    if func_number == 3:
        return (pos[0] - 2) ** 4 + (pos[0] - 2 * pos[1]) ** 2
    if func_number == 4:
        return 4 * (pos[0] - 5) ** 2 + (pos[1] - 6) ** 2
    if func_number == 5:
        return (pos[0] - 2) ** 4 + (pos[0] - 2 * pos[1]) ** 2
    if func_number == 6:
        return 2 * pos[0] ** 3 + 4 * pos[0] * pos[1] ** 3 - 10 * pos[0] * pos[1] + pos[1] ** 2
    if func_number == 7:
        return 8 * pos[0] ** 2 + 4 * pos[0] * pos[1] + 5 * pos[1] ** 2
    if func_number == 8:
        return (pos[1] - pos[0] ** 2) ** 2 + (1 - pos[0]) ** 2
    if func_number == 9:
        return (pos[1] - pos[0] ** 2) ** 2 + 100 * (1 - pos[0] ** 2) ** 2
    if func_number == 10:
        return pos[0] ** 3 + pos[1] ** 2 - 3 * pos[0] - 2 * pos[1] + 2
    if func_number == 11:
        return (pos[0] - 1) ** 2 + (pos[1] - 3) ** 2 + 4 * (pos[2] + 5) ** 2
    if func_number == 12:
        return 3 * (pos[0] - 4) ** 2 + 5 * (pos[1] + 3) ** 2 + 7 * (2 * pos[2] + 1) ** 2


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


